#++++++++++++++++++++++++++++++++++++++++++++++++++++
#	Toolchain Setup
#++++++++++++++++++++++++++++++++++++++++++++++++++++
CROSS :=

CC      = $(CROSS)gcc
CXX     = $(CROSS)gcc
CCDEP   = $(CROSS)gcc
AR      = $(CROSS)ar
LD  	= $(CROSS)gcc

STRIP   = $(CROSS)strip
OBJCOPY = $(CROSS)objcopy
OBJDUMP = $(CROSS)objdump

#++++++++++++++++++++++++++++++++++++++++++++++++++++
#	Compile Path
#++++++++++++++++++++++++++++++++++++++++++++++++++++
SRC_ROOT	:= $(CURDIR)/src

OUT_DIR		:= $(CURDIR)/out
# OBJ_DIR		:= $(OUT_DIR)/obj
LIB_DIR		:= $(OUT_DIR)/lib
EXE_DIR		:= $(OUT_DIR)/exe

#++++++++++++++++++++++++++++++++++++++++++++++++++++
#	Target
#++++++++++++++++++++++++++++++++++++++++++++++++++++
TARGET		:= $(EXE_DIR)/test
TARGET_BIN	:= $(TARGET).bin
TARGET_MAP	:= $(TARGET).map
TARGET_DIS	:= $(TARGET).dis

#++++++++++++++++++++++++++++++++++++++++++++++++++++
#	Source FileList
#++++++++++++++++++++++++++++++++++++++++++++++++++++
SRCS	:= $(shell find $(SRC_ROOT) -name '*.c')

SRCS_OMIT	:= 
ifneq ($(strip $(SRCS_OMIT)),)
SRCS	:= $(filter-out $(SRCS_OMIT),$(SRCS))
endif

#++++++++++++++++++++++++++++++++++++++++++++++++++++
#	Object FileList
#++++++++++++++++++++++++++++++++++++++++++++++++++++
OBJS	:= $(SRCS:%.c=%.o)
# OBJS	:= $(notdir $(OBJS))
# OBJS	:= $(addprefix $(OBJ_DIR)/,$(OBJS))

OBJ_DIR := $(dir $(OBJS))
#++++++++++++++++++++++++++++++++++++++++++++++++++++
#	Compiler Option
#++++++++++++++++++++++++++++++++++++++++++++++++++++
LINK_MAP	= -Wl,-Map=$(TARGET_MAP)

#++++++++++++++++++++++++++++++++++++++++++++++++++++
#	Linker Option
#++++++++++++++++++++++++++++++++++++++++++++++++++++

##########################################################################################
#	set task
##########################################################################################
.PHONY:all clean target
default:all

all: prepare target build_done

clean: out_clean clean_done

##########################################################################################
##task [0] prepare out directory
##########################################################################################
test:
	@echo $(SRCS)
	@echo $(OBJS)
	@echo $(OBJS1)
	@echo $(OBJS2)

prepare:
	@echo "------- task [0] prepare out dir -------"
	# mkdir $(OBJ_DIR) -p
	mkdir $(LIB_DIR) -p
	mkdir $(EXE_DIR) -p
	@echo "";echo ""


$(OBJS) : %.o:%.c
	@echo "------- task [1] complie files -------"
	$(CC) $(VSS_CFLAGS) -Wall -c $< -o $@
	@echo "";echo ""

target:$(OBJS)
	@echo "------- task [2] generate target files -------"
	$(LD) $(LDFLASGS) $(LINK_MAP) -o $(TARGET) $(OBJS) 
	$(OBJDUMP) -D $(TARGET) > $(TARGET_DIS)
	$(OBJCOPY) -O binary $(TARGET) $(TARGET_BIN)

##########################################################################################
#task [4]	clean out
##########################################################################################
out_clean: 
	rm $(OUT_DIR) -rf
	rm src/*.o -rf
	rm src/a/*.o -rf

build_done:
	@echo -e "\033[31;32m [------- build done -------] \033[0m"
	@echo "";echo ""

clean_done:
	@echo -e "\033[31;31m [------- clean done -------] \033[0m"
	@echo "";echo ""




